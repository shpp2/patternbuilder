package shpp.com.example.builder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import shpp.com.example.app.Manual;

public class ManualBuilder implements MyBuilder<Manual> {
    private Manual manual;
    private final Logger logger = LoggerFactory.getLogger(Manual.class);
    private static final String MESSAGE = "do ... {}";

    @Override
    public void contract() {
        this.manual = new Manual();
    }

    @Override
    public void setWall(String material) {
        this.manual.setWallManual(material);
        logger.info(MESSAGE, material);
    }

    @Override
    public void setFlor(String material) {
        this.manual.setFlorManual(material);
        logger.info(MESSAGE, material);
    }

    @Override
    public void setRoof(String material) {
        this.manual.setRoofManual(material);
        logger.info(MESSAGE, material);
    }

    @Override
    public void setWindow(String material) {
        this.manual.setWindowManual(material);
        logger.info(MESSAGE, material);
    }

    @Override
    public void setDoor(String material) {
        this.manual.setDoorManual(material);
        logger.info(MESSAGE, material);
    }

    @Override
    public Manual build() {
        return this.manual;
    }
}
