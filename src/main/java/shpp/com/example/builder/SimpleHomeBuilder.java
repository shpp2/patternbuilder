package shpp.com.example.builder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import shpp.com.example.app.Home;

public class SimpleHomeBuilder implements MyBuilder<Home> {
    private Home home;
    private final Logger logger = LoggerFactory.getLogger(SimpleHomeBuilder.class);
    private static final String MESSAGE = "do ... {}";

    @Override
    public void contract() {
        this.home = new Home();
    }

    @Override
    public Home build() {
        return this.home;
    }

    @Override
    public void setWall(String material) {
        this.home.setWall(material);
        logger.info(MESSAGE, material);
    }

    @Override
    public void setFlor(String material) {
        this.home.setFlor(material);
        logger.info(MESSAGE, material);
    }

    @Override
    public void setRoof(String material) {
        this.home.setRoof(material);
        logger.info(MESSAGE, material);
    }

    @Override
    public void setWindow(String material) {
        this.home.setWindow(material);
        logger.info(MESSAGE, material);
    }

    @Override
    public void setDoor(String material) {
        this.home.setDoor(material);
        logger.info(MESSAGE, material);
    }
}
