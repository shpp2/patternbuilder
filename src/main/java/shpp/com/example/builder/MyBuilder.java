package shpp.com.example.builder;

public interface MyBuilder<T> {
    void contract();

    void setWall(String material);

    void setFlor(String material);

    void setRoof(String material);

    void setWindow(String material);

    void setDoor(String material);

    T build();
}
