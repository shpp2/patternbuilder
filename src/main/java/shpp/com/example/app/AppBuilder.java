package shpp.com.example.app;

import shpp.com.example.builder.ManualBuilder;
import shpp.com.example.builder.SimpleHomeBuilder;

public class AppBuilder {
    public static void main(String[] args) {
        // creating a builder
        SimpleHomeBuilder homeBuilder = new SimpleHomeBuilder();
        ManualBuilder manualBuilder = new ManualBuilder();
        // creation of objects through the director
        Director director = new Director();
        director.constructSimpleHome(homeBuilder);
        // The finished product is returned by the builder,
        // because the director does not know and does not depend on specific classes
        Home myHome = homeBuilder.build();

        director.constructManualForHome(manualBuilder);
        Manual manual = manualBuilder.build();
    }
}
