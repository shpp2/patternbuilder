package shpp.com.example.app;

public class Manual {
    private String wallManual;
    private String florManual;
    private String doorManual;
    private String windowManual;
    private String roofManual;
    public String getWallManual() {
        return wallManual;
    }

    public void setWallManual(String wallManual) {
        this.wallManual = wallManual;
    }

    public String getFlorManual() {
        return florManual;
    }

    public void setFlorManual(String florManual) {
        this.florManual = florManual;
    }

    public String getDoorManual() {
        return doorManual;
    }

    public void setDoorManual(String doorManual) {
        this.doorManual = doorManual;
    }

    public String getWindowManual() {
        return windowManual;
    }

    public void setWindowManual(String windowManual) {
        this.windowManual = windowManual;
    }

    public String getRoofManual() {
        return roofManual;
    }

    public void setRoofManual(String roofManual) {
        this.roofManual = roofManual;
    }
}
