package shpp.com.example.app;

import shpp.com.example.builder.MyBuilder;

public class Director {

    public void constructSimpleHome(MyBuilder builder) {
        builder.contract();
        builder.setFlor("build flor for simple home ...");
        builder.setWall("build wall for simple home ...");
        builder.setRoof("build roof for simple home ...");
        builder.setWindow("build window for simple home ...");
        builder.setDoor("build door for simple home ...");
    }

    public void constructManualForHome(MyBuilder builder){
        builder.contract();
        builder.setFlor("write manual on flor for home ...");
        builder.setWall("write manual on wall for home ...");
        builder.setRoof("write manual on roof for home ...");
        builder.setWindow("write manual on window for home ...");
        builder.setDoor("write manual on door for home ...");
    }
}
